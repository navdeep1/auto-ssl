## Auto SSL Updater:

This is a wrapper script that updates your SSL certificates from letsencrypt few days before they expire.
Script has been written and tested in Ubuntu 14.04/nginx1.4.6.

The script runs everyday and it will renew the certificates 15 days before expiry date.

Email notifications are sent everyday with the status if the ssl certificates were updated or not.

#### Requirements 

- [Certbot](https://github.com/certbot/certbot) must be installed and an ssl certificate must be in place already.
- mail should be configured to send emails. In ubuntu this is installed as mailutils. Eg: ```$apt-get install mailutils -y```


#### Example & Terminology
Run the script as shown:

Eg:

```$ ./sslupdater -c /etc/letsencrypt/live/knowncircle.com/fullchain.pem -e navdeep.singh@example.com -s Knowncircle_SSL_Update```

All the arguments are required.

``` -c <absolute/path/fullchain.pem>``` - Takes the fullchain.pem as its argument 

``` -e <email_address>``` - Email address to send daily notifications

``` -s <subject>``` - Mention the email subject to be used in the notification emails.


<i>Tip: Set the subject related to your domain name as later the emails can be managed via folder filters in your mail box.</i>


#### Setup

- Clone the repository
- Set the execute permissions for the script.
	- 	 ```$ chmod +x sslupdater```
- Place a <b>root</b> cronjob to run the script every day
	-  ```0 0 0 * * * /path/to/sslupdater -c <path/fullchain.pem> -e <email> -s <subject>```

That's it!,Once you have setup this succcessfully, You never have to worry on updating your SSL certificates for your website :+1:
